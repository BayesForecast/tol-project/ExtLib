
//////////////////////////////////////////////////////////////////////////////
Real TimeSetEqual_In(TimeSet ts1, TimeSet ts2, Date begin, Date end) 
//////////////////////////////////////////////////////////////////////////////
{ AreCompatibleTimeSet(ts1, ts2, begin, end) };
//////////////////////////////////////////////////////////////////////////////
PutDescription("Devuelve si un timeset es igual a otro en un determinado "
  "intervalo de tiempo.", TimeSetEqual_In);

//////////////////////////////////////////////////////////////////////////////
Real TimeSetBelong_In(TimeSet ts1, TimeSet ts2, Date begin, Date end) 
//////////////////////////////////////////////////////////////////////////////
{ AreCompatibleTimeSet(ts1, ts2*ts1, begin, end) };
//////////////////////////////////////////////////////////////////////////////
PutDescription("Devuelve si un timeset contiene a otro en un determinado "
  "intervalo de tiempo, es decir, si todos los instantes del primer timeset"
  "en dicho intervalo est�n contenidos en el segundo.", TimeSetBelong_In);

//////////////////////////////////////////////////////////////////////////////
// DateSet: TimeSet cuyas fechas pertenecen a C (sus instantes son fechas)
// Para facilitar las funcionalidades asociadas, se admite que son fechas
// todas aquellas entre: DateSetBegin y DateSetEnd

//////////////////////////////////////////////////////////////////////////////
Date DateSetBegin = y1901;
Date DateSetEnd = y2095m12d31;
//////////////////////////////////////////////////////////////////////////////
Real IsDateSet(TimeSet timeSet)  
//////////////////////////////////////////////////////////////////////////////
{ TimeSetBelong_In(timeSet, C, DateSetBegin, DateSetEnd) };
//////////////////////////////////////////////////////////////////////////////
PutDescription("Devuelve si un timeset es un fechado, en el sentido de que "
  "todos sus instante son fechas propiamente dichas, es decir los instantes "
  "tienen hora, minuto y segundo ceros. "
  "Por defecto s�lo se comprueba entre las fechas DateSetBegin y DateSetEnd.", 
  IsDateSet);

//////////////////////////////////////////////////////////////////////////////
Real IsHarmonicOf(TimeSet ts1, TimeSet ts2) 
//////////////////////////////////////////////////////////////////////////////
{
  Case(Not(IsDateSet(ts1)), {
    WriteLn("No se tratan timesets con instantes inferiores al d�a.", "E");
  ?}, Not(IsDateSet(ts2)), {
    WriteLn("No se tratan timesets con instantes inferiores al d�a.", "E");
  ?}, TimeSetBelong_In(ts2, ts1, DateSetBegin, DateSetEnd), {
    Serie s = SubSer(CalVar(ts1,ts2), DateSetBegin, DateSetEnd);
    Real period = MinS(s);
    If(period==MaxS(s), period, 0)
  }, True, 0)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Obtiene si un fechado es arm�nico de otro, es decir, "
  "si el otro est� contenido en el primero y entre dos fechas del "
  "segundo hay siempre un determinado n�mero de fechas del primero. "
  "En caso de ser as� se devuelve dicho n�mero (m�s 1) que podemos denominar "
  "orden o periodicidad del arm�nico, en otro caso, se devuelve 0.\n"
  "Por ejemplo:\n"
  "Real IsHarmonicOf(Monthly, Yearly); // -> 12", 
  IsHarmonicOf);
  
//////////////////////////////////////////////////////////////////////////////
Real TimeSetPeriod(TimeSet dating, TimeSet reference) 
//////////////////////////////////////////////////////////////////////////////
{ AvrS(CalVar(dating, reference), DateSetBegin, DateSetEnd) };

//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Real TimeSet.Compare(Anything dating, Anything datingOrig)
//////////////////////////////////////////////////////////////////////////////
{
  //  0: fechado de dating y de datingOrig iguales.
  //  1: fechado de datingOrig m�s fino que el de dating.
  // -1: fechado de dating m�s fino o incompatible con el de datingOrig.
  Text gramDating = Grammar(dating);
  Text gramDatingOrig = Grammar(datingOrig);

  @TimeSet datingModS = Case(
    gramDating=="TimeSet", @TimeSet(dating);
    gramDating=="Text", @TimeSet(Eval(dating));
    gramDating=="Set",  dating
  );
  @TimeSet datingOriS = Case(
    gramDatingOrig=="TimeSet", @TimeSet(datingOrig);
    gramDatingOrig=="Text", @TimeSet(Eval(datingOrig));
    gramDatingOrig=="Set",  datingOrig
  );
  Real  If(
    TimeSetBelong_In($datingModS, $datingOriS, DateSetBegin, DateSetEnd),
    If(
      TimeSetBelong_In($datingOriS, $datingModS, DateSetBegin, DateSetEnd), 
      0, 
      1
    ), 
    -1
  )
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
  "   0: fechado de dating y de datingOrig iguales."+
  "   1: fechado de datingOrig m�s fino que el de dating."+
  "  -1: fechado de dating m�s fino o incompatible con el de datingOrig.",  
   TimeSet.Compare);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text TimeSet.StandardName(TimeSet ts)
//////////////////////////////////////////////////////////////////////////////
{
  Date begin = y1901;
  Date end = y2097;
  Real n = DateDif(ts, begin, end); // 49*4 years
  Text name = If(n==0, "#W", {
    Date first = Succ(Succ(begin, ts, -1), ts, 1);
    Date last = Succ(Succ(end, ts, 1), ts, -1);
    Real m = DateDif(ts, first, last);
    //Real nC = DateDif(C, begin, end);
    Real mC = DateDif(C, first, last);
    // ... < MaxS(SubSer(CalVar(C, <Candidate>), y2000, y2009));
    Case(mC<1461, {
      // Fechado acotado de menos de 4 a�os.
      "#L"
    }, m>mC, {
      "#E"
    }, m==mC, {
      If(AreCompatibleTimeSet(ts, Daily, first, last), "Daily", "#C")
    }, Real Abs((m+1)*(365.25/12)-(mC+1)) < 31, {
      Case(AreCompatibleTimeSet(ts, Monthly, first, last), "Monthly", 
        AreCompatibleTimeSet(ts, Monthly445, first, last), "Monthly445",  
        True, "#M")
    }, Real Abs((m+1)*365.25-(mC+1)) < 366, {
      Case(AreCompatibleTimeSet(ts, Yearly, first, last), "Yearly", 
        AreCompatibleTimeSet(ts, Easter, first, last), "Easter",
        True, "#Y")
    }, Real Abs((m+1)*7-(mC+1)) < 7, {
      Case(AreCompatibleTimeSet(ts, WD(1), first, last), "Weekly",
        AreCompatibleTimeSet(ts, WD(7), first, last), "Sundays",
        AreCompatibleTimeSet(ts, WD(6), first, last), "Saturdays",
        AreCompatibleTimeSet(ts, WD(5), first, last), "Fridays",
        AreCompatibleTimeSet(ts, WD(4), first, last), "Thursdays",
        AreCompatibleTimeSet(ts, WD(3), first, last), "Wednesdays",
        AreCompatibleTimeSet(ts, WD(2), first, last), "Tuesdays", 
        AreCompatibleTimeSet(ts, Weekly445, first, last), "Weekly445",         
        True, "#WD")
    }, Real Abs((m+1)*(7/5)-(mC+1)) < (7-5+1), {
      If(AreCompatibleTimeSet(ts, WD(1)+WD(2)+WD(3)+WD(4)+WD(5), first, last), "Workdays", "#WD5")
    }, Real Abs((m+1)*(7/2)-(mC+1)) < (7-2+1), {
      If(AreCompatibleTimeSet(ts, WD(6)+WD(7), first, last), "Weekends", "#WD2")
    }, Real Abs((m+1)*(365.25/4)-(mC+1)) < 92, {
      Case(AreCompatibleTimeSet(ts, Quarterly, first, last), "Quarterly", 
        AreCompatibleTimeSet(ts, Quarterly445, first, last), "Quarterly445",       
        True, "#Q")
    }, Real Abs((m+1)*(365.25/2)-(mC+1)) < 184, {
      If(AreCompatibleTimeSet(ts, HalfYearly, first, last), "HalfYearly", "#HY")
    }, Real Abs((m+1)*(365.25/24)-(mC+1)) < 16, {
      If(AreCompatibleTimeSet(ts, D(1)+D(16), first, last), "HalfMonthly", "#HM")
    }, Real Abs((m+1)*(365.25/36)-(mC+1)) < 11, {
      If(AreCompatibleTimeSet(ts, D(1)+D(11)+D(21), first, last), "ThirdMonthly", "#3M")
    }, Real Abs((m+1)*(365.25/3)-(mC+1)) < 123, {
      If(AreCompatibleTimeSet(ts, (M(1)+M(5)+M(9))*D(1), first, last), "FourMonthly", "#4M")
    }, Real Abs((m+1)*(365.25/6)-(mC+1)) < 62, {
      If(AreCompatibleTimeSet(ts, (M(1)+M(3)+M(5)+M(7)+M(9)+M(11))*D(1), first, last), "TwoMonthly", "#2M")
    }, mC<1461*3, {
      //WriteLn("Fechado acotado de menos de 4 a�os.", "W");
      "#L2"
    }, Real Abs((m+1)*(365.25*2)-(mC+1)) < (365+366), {
      Case(AreCompatibleTimeSet(ts, Periodic(y1584, 2, Yearly), first, last), "TwoYearly", 
        AreCompatibleTimeSet(ts, Periodic(y1583, 2, Yearly), first, last), "TwoYearly1", 
        True, "#2Y")
    }, Real Abs((m+1)*(365.25/52)-(mC+1)) < 12, {
      Case(AreCompatibleTimeSet(ts, Weekly445, first, last), "Weekly445",       
        True, "#WD")
    }, Real Abs((m+1)*(365.25/12)-(mC+1)) < 40, {
      Case(AreCompatibleTimeSet(ts, Monthly445, first, last), "Monthly445",       
        True, "#WD")
    }, Real Abs((m+1)*(365.25/4)-(mC+1)) < 96, {
      Case(AreCompatibleTimeSet(ts, Quarterly445, first, last), "Quarterly445",       
        True, "#WD")
    }, True, {
      "#?"
    })
  });
  If(TextSub(name,1,1)=="#", "", name)
};
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Code  Dating.StandardName = TimeSet.StandardName;
//////////////////////////////////////////////////////////////////////////////

Set _TimeSetStandard = [[ 
  "Daily", 
  "Workdays", 
  "Weekends", 
  "Thursdays", 
  "Weekly", 
  "Sundays", 
  "Saturdays", 
  "Fridays", 
  "Wednesdays", 
  "Tuesdays", 
  "Weekly445", 
  "ThirdMonthly", 
  "HalfMonthly", 
  "Monthly", 
  "Monthly445", 
  "TwoMonthly", 
  "Quarterly", 
  "Quarterly445", 
  "FourMonthly", 
  "HalfYearly", 
  "Yearly"
]];
//////////////////////////////////////////////////////////////////////////////
Set TimeSet.Harmonics(Anything anyTs)
//////////////////////////////////////////////////////////////////////////////
{
  TimeSet ts = Case(
    Grammar(anyTs)=="Text", Eval(anyTs),
    Grammar(anyTs)=="TimeSet", anyTs,
    Grammar(anyTs)=="Serie", Dating(anyTs), 
    Grammar(anyTs)=="Set", Case(
        Grammar(anyTs[1])=="Text", Eval(anyTs[1]),
        Grammar(anyTs[1])=="TimeSet", anyTs[1],
        Grammar(anyTs[1])=="Serie", Dating(anyTs[1])
      )
  );

  Text dating = TimeSet.StandardName(ts);
  If( 
    dating == "Daily", 
	[[ "Daily", "Weekly", "Monthly",  "Yearly"]],
    Select(_TimeSetStandard, Real(Text t)
    {
      Real r = TimeSet.Compare(t, ts);
      r != -1
    })
  )
}; 


