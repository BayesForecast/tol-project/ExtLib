
Real _fun_serie = 1;

//////////////////////////////////////////////////////////////////////////////
Real IsLimitedSerie(Serie serie) 
//////////////////////////////////////////////////////////////////////////////
{ IsFiniteDate(First(serie)) & IsFiniteDate(Last(serie)) };
//////////////////////////////////////////////////////////////////////////////
PutDescription("Devuelve cierto si una serie est� limitada temporalmente, "
  "es decir, sus fechas inicio y fin son finitas.", IsLimitedSerie);

//////////////////////////////////////////////////////////////////////////////
Serie DeltaTransform(Serie serie, Real delta)
//////////////////////////////////////////////////////////////////////////////
{
  Case(!IsLimitedSerie(Serie serie), {
    WriteLn("No puede integrarse una serie ilimitada.", "E");
    Serie serie * ?  
  }, Not(MinS(IsFinite(serie))), {
    WriteLn("No puede integrarse una serie con valores no finitos.", "E");
    Serie serie * ?
  }, True, {
    Serie DifEq(Ratio (1-delta)/(1-delta*B), serie, 0)
  })
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Integra una serie usando un ratio de tipo delta "
  "con ganancia unitaria: Ratio (1-delta)/(1-delta*B) y 0 como constante.", 
  DeltaTransform);

//////////////////////////////////////////////////////////////////////////////
Serie DatChInf(Serie serie, TimeSet timeSet, Code statistic)
//////////////////////////////////////////////////////////////////////////////
{
  // DatCh no considera, en un cambio de fechado, las fechas del 
  // nuevo fechado que se encuentran entre la �ltima fecha de la serie 
  // y la siguiente en el fechado original.
  Date next = Succ(Last(serie), Dating(serie), 1);
  // Se alarga la serie original (con un omitido para que no interfiera
  // y otro valor - un 0 en este caso - para que no se elimine el omitido)
  // para que as� DatCh incorpore de manera natural estas �ltimas fechas.
  Date next2 = Succ(next, Dating(serie), 1);
  Date last = Succ(next, timeSet, -1);
  SubSer(DatCh(serie<<SubSer(CalInd(W, Dating(serie)), next2, next2), 
    timeSet, statistic), TheBegin, last)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Variante del m�todo DatCh que permite hacer un cambio de "
  "fechado a un fechado inferior: un fechado que contiene todas las fechas "
  "del fechado original. A diferencia de DatCh, incorpora las fechas "
  "del nuevo fechado que se encuentran entre la �ltima fecha de la serie "
  "y la siguiente en el fechado original.\nPor ejemplo, en combinaci�n "
  "con PreviousS nos permite extender una serie:\n"
  "Serie s = SubSer(CalVar(Daily, Monthly), y2000, y2000m12);\n"
  "Serie DatChInf(s, Daily, PreviousS);", DatChInf);

//////////////////////////////////////////////////////////////////////////////
Real PreviousS(Serie serie, Date begin, Date end)
//////////////////////////////////////////////////////////////////////////////
{ SerDat(serie, DateFloor(begin, Dating(serie))) };
//////////////////////////////////////////////////////////////////////////////
PutDescription("Devuelve el valor de la serie en el comienzo del intervalo "
  "'begin' o en su defecto el valor de un instante anterior.\n"
  "Por ejemplo, en combinaci�n con DatChInf nos permite extender una serie:\n"
  "Serie s = SubSer(CalVar(Daily, Monthly), y2000, y2000m12);\n"
  "Serie DatChInf(s, Daily, PreviousS);", PreviousS);

//////////////////////////////////////////////////////////////////////////////
Real BeginS(Serie serie, Date begin, Date end) 
//////////////////////////////////////////////////////////////////////////////
{ SerDat(serie, begin) };
//////////////////////////////////////////////////////////////////////////////
PutDescription("Devuelve el valor de la serie en el primer instante del "
  "intervalo, aun cuando �ste sea desconocido. "
  "N�tese que por contra, FirstS devuelve el primer valor de la serie "
  "en el intervalo, aunque esta fecha no sea "
  "necesariamente la primera del intervalo.", BeginS);

//////////////////////////////////////////////////////////////////////////////
Real EndS(Serie serie, Date begin, Date end) 
//////////////////////////////////////////////////////////////////////////////
{ SerDat(serie, end) };
//////////////////////////////////////////////////////////////////////////////
PutDescription("Devuelve el valor de la serie en el �ltimo instante del "
  "intervalo, aun cuando �ste sea desconocido. "
  "N�tese que por contra, LastS devuelve el �ltimo valor de la serie "
  "en el intervalo, aunque esta fecha no sea "
  "necesariamente la �ltima del intervalo.", EndS);

//////////////////////////////////////////////////////////////////////////////
Serie CopySerie(Serie serie) 
//////////////////////////////////////////////////////////////////////////////
{
  If(IsLimitedSerie(serie), {
    If(ObjectExist("TimeSet", DatingName(serie)), {
      Serie copy = MatSerSet(SerMat(serie), 
        Eval(DatingName(serie)), First(serie))[1];
      PutName(Name(serie), copy)
    }, {
      Serie copy = Copy(serie);
      PutName(Name(serie), copy)
    })
  }, serie)
};

//////////////////////////////////////////////////////////////////////////////
Serie SubSerie(Serie serie, Date begin, Date end) 
//////////////////////////////////////////////////////////////////////////////
{ CopySerie(SubSer(serie, begin, end)) };

//////////////////////////////////////////////////////////////////////////////
Serie Reverse2000_Serie(Serie serie)
//////////////////////////////////////////////////////////////////////////////
{
  If(IsLimitedSerie(serie), {
    Matrix m = Reverse(SerMat(serie));
    Date dt = Succ(y2000, Dating(serie), -DateDif(Dating(serie), y2000, Last(serie)));
    MatSerSet(m, Dating(serie), dt)[1]
  }, {
    WriteLn("No pueden invertir el sentido de una serie ilimitada.", "E");
    If(False, ?)  
  })
};

//////////////////////////////////////////////////////////////////////////////
